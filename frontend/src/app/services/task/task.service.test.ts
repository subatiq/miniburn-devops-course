import { TaskService } from './task.service';
import { HttpClient } from '@angular/common/http';
import { API } from '../api/api.service';

describe('Task Service', () => {
  it('should add headers to each request', () => {
    const http = new HttpClient(undefined);
    const api = new API();
    const taskService = new TaskService(http, api);
    expect(taskService.getHeaders().keys()).toBeTruthy();
  });
});
