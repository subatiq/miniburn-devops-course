# Miniburn
## Prerequisites
- Python 3.8.x
- Node.js 14.x
- Docker 19.x
- docker-compose 1.26.2

## Installation steps
The solution relies on the cloud-based GitLab VCS and CI/CD services. It doesn’t mean one couldn’t recreate the same deployment pipeline on the local GitLab server. Here we provide steps to recreate the pipeline based on cloud solution.

Important! Installation steps don't include setting up the git hooks. They are based on husky node package and would require the document of the same size to describe.

1. Go to https://gitlab.com and sign in/sign up into your account. 

2. Go to https://gitlab.com/sepsseven/Miniburn and click «Fork» button.

3. Now go to your fork and clone the repository to your local machine.
        
        ```cd <your working directory>
        git clone <address_to_your_fork>```

4. To install all of the development dependencies run the command:
        
        `./miniburn deploy front dev`
        
        This will install npm and pip dependencies needed for development

5. Now to run the application run these commands:

        ```sudo mongod
        cd flask-api & flask run
        cd ../server & flask run -p 8000```

This will start database, API and application respectively.

6. Go to http://localhost:8000 and check if the app is running

7. To create a CI/CD pipeline first create a branch in your GitLab repository: go to Repository > Branches > New branch. Enter the name of the new branch, for example `feature/ci-cd-pipline`.

8. In the terminal on your local machine make sure you are in the repository directory and switch branch:

```cd <your working directory>
git fetch
git checkout feature/ci-cd-pipline```

9. Create a new file called `.gitlab-ci.yml` and copy paste into it the following:

```
image: "nikolaik/python-nodejs:python3.8-nodejs14-alpine"

stages:
  - build
  - test
  - integration
  - production

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  key: "dependencies"
  paths:
    - .cache/pip
    - venv/
    - node_modules

build:
  stage: build
  script:
    - echo "Building"
    - npm install
    - npm install @angular/cli
    - pip install virtualenv
    - ./create-venv
    - source venv/bin/activate
    - pip install wheel
    - ./miniburn deploy front test
    - ./miniburn build front prod
  artifacts:
    paths:
      - server/templates
      - server/static

test-frontend:
  image: "node:14-alpine3.12"
  stage: test
  script:
    - echo "Testing frontend"
    - npm run test
  cache:
    key: "dependencies"
    policy: pull
    paths:
      - node_modules


test-backend:
  image: "python:3.8-alpine"
  stage: test
  script:
    - echo "Testing backend"
    - source venv/bin/activate
    - python -m pytest
  cache:
    key: "dependencies"
    policy: pull
    paths:
      - .cache/pip
      - venv/

frontend-lint:
  image: "node:14-alpine3.12"
  stage: test
  script:
    - echo "Lint frontend"
    - npm run lint:ts
  cache:
    key: "dependencies"
    policy: pull
    paths:
      - node_modules

backend-lint:
  image: "python:3.8-alpine"
  stage: test
  script:
    - echo "Lint backend"
    - source venv/bin/activate
    - python -m pylint -d duplicate-code server
    - python -m pylint -d duplicate-code flask-api
  cache:
    key: "dependencies"
    policy: pull
    paths:
      - .cache/pip
      - venv/

test-integration:
  image: docker/compose:latest
  services:
    - docker:dind
  stage: integration
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  only:
    - master
  script:
    - echo "Integration test"
    - docker-compose version
    - docker-compose up --abort-on-container-exit
```

10. Create a new file called `config.toml` to make sure GitLab will run containers in "Docker in Docker" mode. Paste the following into the file:

```
[[runners]]
  environment = ["DOCKER_TLS_CERTDIR="]
  [runners.docker]
    privileged = true
```

11. Now commit changes to the repository: 
``` 
git add .
git commit -m "add pipeline configuration"
git push
```

You'll find your pipeline going on GitLab in CI/CD > Pipelines.

### Adding deployment to the pipeline

For deployment we used Heroku cloud service.

12. To add deployment process to the pipeline sign in/sign up into your Heroku account and create two new apps with servers based in Europe. One for frontend and the second one for API. Name it as you see fit.

13. Go to your `.gitlab-ci.yml` file and add the following:

        ``` 
        deploy-app:
  type: deploy
  stage: production
  image: ruby:latest
  script:
    - mv server/Procfile.server Procfile
    - mv server/dependencies/prod.txt requirements.txt
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=miniburn --api-key=$HEROKU_API_KEY --skip_cleanup
  only:
    - master

deploy-api:
  type: deploy
  stage: production
  image: ruby:latest
  script:
    - mv flask-api/Procfile.api Procfile
    - mv flask-api/dependencies/prod.txt requirements.txt
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=miniburn-api --api-key=$HEROKU_API_KEY --skip_cleanup
  only:
    - master
      
      ```

14. Go to frontend/src/environments and edit line 4 in environment.prod.ts to

```api: <host-of-your-api-app>,```

15. Commit changes to the repository: 
``` 
git add .
git commit -m "add pipeline configuration"
git push
```

16. Merge your branch into the master. Now deployment will queue right after the integration testing stage. Wait till it finish.

17. Go to your frontend Heroku application and press "Open app". If everything went successful you should see the application running.

The application is still pretty unpolished and certain features might not work.






